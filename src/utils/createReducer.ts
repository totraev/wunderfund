export type Action<P> = {
  type: string
  payload?: P
  error?: boolean
};

export type Reducer<S, P> = (state: S, action: Action<P>) => S;

export type HandlersMap<S, P> = {
  [actionType: string]: Reducer<S, P>
};


function createReducer<S>(handlers: HandlersMap<S, any>, initialState: S): Reducer<S, any> {
  return (state: S = initialState, action: Action<any> = null): S => {
    return handlers[action.type]
      ? handlers[action.type](state, action)
      : state;
  };
}


export default createReducer;

