import { RBTree } from 'bintrees';
import BigNumber from 'bignumber.js';


type Order = {
  id: string
  side: 'buy' | 'sell'
  price: BigNumber
  size: BigNumber
};

type OrderState = {
  id: string
  side: 'buy' | 'sell'
  price: number
  size: number
};

type State = {
  productId: string
  spread: number
  asks: OrderState[]
  bids: OrderState[]
};


class OrderBook {
  private bids: RBTree<Order>;
  private asks: RBTree<Order>;
  private productId: string;

  constructor(productId: string) {
    this.productId = productId;
    this.bids = new RBTree((a, b) => a.price.comparedTo(b.price));
    this.asks = new RBTree((a, b) => a.price.comparedTo(b.price));
  }

  private getTree(side) {
    return side === 'buy' ? this.bids : this.asks;
  }

  get state(): State {
    /*
    const asks: OrderState[] = [];
    const bids: OrderState[] = [];

    this.bids.reach(({ id, size, price, side }) => bids.push({
      id,
      side,
      price: price.toNumber(),
      size: size.toNumber()
    }));

    this.asks.reach(({ id, size, price, side }) => asks.push({
      id,
      side,
      price: price.toNumber(),
      size: size.toNumber()
    }));
    */
    const spread = this.asks.min().price.minus(this.bids.max().price);

    return { 
      asks: this.asksState,
      bids: this.bidsState,
      productId: this.productId,
      spread: spread.toNumber()
    };
  }

  get asksState(): OrderState[] {
    const it = this.asks.iterator();
    const asks: OrderState[] = [];
    let item: Order;

    while ((item = it.next()) !== null && asks.length < 50) {
      asks.push({
        id: item.id,
        side: item.side,
        price: item.price.toNumber(),
        size: item.size.toNumber()
      });
    }

    return asks;
  }

  get bidsState(): OrderState[] {
    const it = this.bids.iterator();
    const bids: OrderState[] = [];
    let item: Order;

    while ((item = it.prev()) !== null && bids.length < 50) {
      bids.push({
        id: item.id,
        side: item.side,
        price: item.price.toNumber(),
        size: item.size.toNumber()
      });
    }

    return bids;
  }

  update(order: Order) {
    if (order.size.isZero()) {
      this.remove(order);
    } else {
      this.add(order);
    }
  }

  add(order: Order) {
    const tree = this.getTree(order.side);
    const node = tree.find(order);

    if (node) {
      node.size = order.size;
    } else {
      tree.insert(order);
    }
  }

  remove(order: Order) {
    this.getTree(order.side).remove(order);
  }
}

export default OrderBook;
