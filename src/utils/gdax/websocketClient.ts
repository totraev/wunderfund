import { EventEmitter } from 'events';


export interface Message {
  type: string;
}

export interface SubscribePayload {
  product_ids?: string[];
  channels?: string[];
}

export interface SubscribeMessage extends Message, SubscribePayload {}

export interface ErrorMessage extends Message {
  message: string;
}


class WebsocketClient extends EventEmitter {
  private productIDs: string[];
  private socket: WebSocket;
  private websocketURI: string;
  private channels: string[];

  constructor(
    productIDs: string[] = ['BTC-USD'],
    websocketURI: string = 'wss://ws-feed.gdax.com',
    channels: string[] = ['level2']
  ) {
    super();

    this.productIDs = productIDs;
    this.websocketURI = websocketURI;
    this.channels = channels;
    
    if (!this.channels.includes('heartbeat')) {
      this.channels.push('heartbeat');
    }
  }

  connect() {
    if (this.socket) {
      this.socket.close();
    }

    this.socket = new WebSocket(this.websocketURI);

    this.socket.onmessage = this.handleMessage;
    this.socket.onopen = this.handleOpen;
    this.socket.onclose = this.handleClose;
    this.socket.onerror = this.handleError;
  }

  disconnect() {
    if (!this.socket) {
      throw new Error('Could not disconnect (not connected)');
    }
    this.socket.close();
    this.socket = null;
  }

  subscribe({ product_ids, channels }: SubscribePayload) {
    this.sendSubscription('subscribe', { product_ids, channels });
  }

  unsubscribe({ product_ids, channels }: SubscribePayload) {
    this.sendSubscription('unsubscribe', { product_ids, channels });
  }

  protected onMessage(message: Message) {
    if (message.type === 'error') {
      this.onError(message as ErrorMessage);
    } else {
      this.emit('message', message);
    }
  }

  protected onError(err: ErrorMessage) {
    if (!err) {
      return;
    }

    if (err.message === 'unexpected server response (429)') {
      throw new Error(
        'You are connecting too fast and are being throttled!\
        Make sure you subscribe to multiple books on one connection.'
      );
    }

    this.emit('error', err);
  }

  private handleOpen = () => {
    this.emit('open');
    this.subscribe({ product_ids: this.productIDs, channels: this.channels });
  }

  private handleClose = ({ wasClean, reason }: CloseEvent) => {
    this.socket = null;
    this.emit('close');

    if (!wasClean) {
      this.onError({ type: 'error', message: reason });
    }
  }

  private handleMessage = ({ data }: MessageEvent) => {
    this.onMessage(JSON.parse(data));
  }

  private handleError = (e: any) => {
    this.onError({ type: 'error', message: e.message });
  }

  private sendSubscription(type: string, data: SubscribePayload) {
    const message: SubscribeMessage = { type };
    const { product_ids, channels } = data;

    if (channels) {
      message.channels = channels;
    }

    if (product_ids) {
      message.product_ids = product_ids;
    }

    this.socket.send(JSON.stringify(message));
  }
}

export default WebsocketClient;
