import { Middleware } from 'redux';
import OrderBookSync from './orderBookSync';
import { updateState } from '../../redux/modules/app';

import { CONNECT, DISCONNECT } from './actions';


const orderBookMiddleware: Middleware = (store) => {
  const orderBook = new OrderBookSync();
  
  orderBook.on('sync', (state) => {
    store.dispatch(updateState(state));
  });
  
  return next => (action) => {
    if (/^@@orderBook/.test(action.type)) {
      switch (action.type) {
        case CONNECT:
          orderBook.connect();
          break;
        case DISCONNECT:
          orderBook.disconnect();
          break;  
      }
    }

    return next(action);
  };
};

export default orderBookMiddleware;
