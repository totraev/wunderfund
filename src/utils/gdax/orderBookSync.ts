import BigNumber from 'bignumber.js';
import throttle from 'lodash.throttle';
import WebsocketClient from './websocketClient';
import OrderBook from './orderBook';


export type SnapshotMessage = {
  type: 'snapshot',
  product_id: string
  bids: [string, string][],
  asks: [string, string][]
};

export type UpdateMessage = {
  type: 'l2update'
  product_id: string
  time: string
  changes: [['buy' | 'sell', string, string]]
};

export type HeartbeatMessage = {
  type: 'heartbeat'
  product_id: string
  time: string
  sequence: number
  last_trade_id: number
};

export type Message = HeartbeatMessage | UpdateMessage | SnapshotMessage;


class OrderBookSync extends WebsocketClient {
  private orderBooks: { [productId: string]: OrderBook } = {};
  private syncTimeout: number = 1000;

  constructor(
    productIDs: string[] = ['BTC-USD'],
    websocketURI: string = 'wss://ws-feed.gdax.com',
    channels: string[] = ['level2']
  ) {
    super(productIDs, websocketURI, channels);

    this.on('message', this.processMessage);
  }

  private processMessage = (msg: Message) => {
    switch (msg.type) {
      case 'l2update':
        return this.updateOrderBook(msg);
      case 'snapshot':
        return this.initOrderBook(msg);
    }
  }

  private initOrderBook({ asks, bids, product_id }: SnapshotMessage) {
    const book = new OrderBook(product_id);
    this.orderBooks[product_id] = book;

    asks.forEach(([price, size]) => {
      book.add({
        id: price,
        price: new BigNumber(price),
        size: new BigNumber(size),
        side: 'sell'
      });
    });

    bids.forEach(([price, size]) => {
      book.add({
        id: price,
        price: new BigNumber(price),
        size: new BigNumber(size),
        side: 'buy'
      });
    });
  }

  private updateOrderBook({ changes, product_id }: UpdateMessage) {
    const [side, price, size] = changes[0];

    const order = {
      side,
      id: price,
      price: new BigNumber(price),
      size: new BigNumber(size)
    };

    this.orderBooks[product_id].update(order);
    this.syncStores(product_id);
  }

  private syncStores = throttle((id: string) => {
    this.emit('sync', this.orderBooks[id].state);
  }, this.syncTimeout);
}

export default OrderBookSync;
