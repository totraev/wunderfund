import { Action } from '../../utils/createReducer';

/**
 * Constants
 */
export const CONNECT    = '@@orderBook/CONNECT';
export const DISCONNECT = '@@orderBook/DISCONNECT';

/**
 * Action creators
 */
export function connectOrderBook(): Action<void> {
  return {
    type: CONNECT
  };
}

export function disconnectOrderBook(): Action<void> {
  return {
    type: DISCONNECT
  };
}
