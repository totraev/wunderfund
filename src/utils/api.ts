export const apiFetch = (path: string, options: any  = {}) => {
  return fetch(path, {
    ...defaultOptions,
    ...options,
    headers: {
      ...defaultOptions.headers,
      ...options.headers
    }
  })
    .then(checkHttpStatus)
    .then(parseJSON);
};


export const checkHttpStatus = (response: Response): Response => {
  if (response.status >= 200 && response.status < 400) {
    return response;
  }

  const error = new Error(response.statusText);
  throw error;
};


export const parseJSON = (response: Response) => response.json();


export const defaultOptions = {
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
};
