import * as React from 'react';
import { Props } from './types';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import './styles.sss';
const { enter, exit } = require('./styles.sss');


const OrderTable: React.SFC<Props> = ({ orders }) => {
  const renderPrice = (price: number): JSX.Element => {
    const [w, d] = price.toFixed(2).split('.');

    return <td styleName="price">
      {w}.<span styleName="fractional">{d}</span>
    </td>;
  };

  const renderSize = (size: number): JSX.Element => {
    const result = size.toFixed(8).match(/^(\d+\.(?:\d*[1-9])?)(0*)$/);

    return <td styleName="size">
      {result[1]}<span styleName="zeroes">{result[2]}</span>
    </td>;
  };

  const renderTotal = (size: number, price: number): JSX.Element => (
    <td styleName="total">{(size * price).toFixed(2)}</td>
  );

  return (
    <table styleName="order-table">
      <thead>
        <tr>
          <th>Size</th>
          <th>Price</th>
          <th>Total</th>
        </tr>
      </thead>
      <TransitionGroup component="tbody" enter exit appear={false}>
        {orders.map(({ id, side, price, size }) => (
          <CSSTransition key={`${side}-${id}`} timeout={500} classNames={{ enter, exit }}>
            <tr styleName={side}>
              {renderSize(size)}
              {renderPrice(price)}
              {renderTotal(size, price)}
            </tr>
          </CSSTransition>
        ))}
      </TransitionGroup>
    </table>
  );
};

OrderTable.defaultProps = {
  orders: []
};

export default OrderTable;

export * from './types';
