export type Order = {
  id: string
  side: 'buy' | 'sell'
  price: number
  size: number
};

export type Props = {
  orders: Order[]
};
