import * as React from 'react';
import { Link } from 'react-router-dom';
import './styles.sss';

const StaticPage: React.SFC = () => (
  <div styleName="static-page">
    <img styleName="logo" src={require('./images/logo.png')} alt=""/>
    <img styleName="name" src={require('./images/wf.png')} alt=""/>

    <p styleName="promo">Our world class trading platform <br/> is comming soon!</p>

    <Link styleName="link" to="/orderbook">View order book</Link>
  </div>
);

export default StaticPage;
