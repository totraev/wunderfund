import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import Loadable from 'react-loadable';
import './styles.sss';

import Loader from '../../components/Loader';

const StaticPage = Loadable({
  loader: () => import(
    /* webpackChunkName: "static-page" */
    '../StaticPage'
  ),
  loading: () => <Loader loading/>
});

const OrderBook = Loadable({
  loader: () => import(
    /* webpackChunkName: "order-book" */
    '../../containers/OrderBook'
  ),
  loading: () => <Loader loading/>
});


class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <div styleName="container">
          <Switch>
            <Route exact path="/" component={StaticPage}/>
            <Route path="/orderbook" component={OrderBook}/>
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
