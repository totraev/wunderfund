import Immutable from 'seamless-immutable';
import { AppState, ImmutableState } from './types';
import createReducer, { Action } from '../../../utils/createReducer';

/**
 * Constans
 */
const UPDATE_STATE    = 'app/UPDATE_STATE';
const INC_AGGREGATION = 'app/INC_AGGREGATION';
const DEC_AGGREGATION = 'app/DEC_AGGREGATION';
const SET_LOADER      = 'app/SET_LOADER';

/**
 * Actions creators
 */
export function updateState(stateSlice: Partial<AppState>): Action<Partial<AppState>> {
  return {
    type: UPDATE_STATE,
    payload: stateSlice
  };
}

export function incAggr(): Action<void> {
  return {
    type: INC_AGGREGATION
  };
}

export function decAggr(): Action<void> {
  return {
    type: DEC_AGGREGATION
  };
}

export function setLoader(loader: boolean): Action<boolean> {
  return {
    type: SET_LOADER,
    payload: loader
  };
}

/** 
 * App reducer
*/
const initialState = Immutable.from<AppState>({
  productId: 'BTC-USD',
  loading: true,
  spread: 0,
  aggregation: {
    values: [0.01, 0.05, 0.1, 0.25, 0.5, 1, 2.5, 5],
    current: 0
  },
  asks: [],
  bids: []
});

export default createReducer<ImmutableState>({
  [UPDATE_STATE]: (state, { payload }: Action<Partial<AppState>>) => (
    state.merge(payload).set('loading', false)
  ),

  [INC_AGGREGATION]: (state) => {
    const { values, current } = state.aggregation;
    const step = current < values.length - 1
      ? current + 1
      : current;

    return state.setIn(['aggregation', 'current'], step);
  },

  [DEC_AGGREGATION]: (state) => {
    const { current } = state.aggregation;
    const step = current > 0
      ? current - 1
      : current;

    return state.setIn(['aggregation', 'current'], step);
  },

  [SET_LOADER]: (state, { payload }: Action<boolean>) => state.set('loading', payload)
}, initialState);

export * from './types';
