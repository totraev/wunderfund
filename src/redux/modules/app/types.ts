import { ImmutableObject } from 'seamless-immutable';

/**
 * Types
 */
export type Order = {
  id: string
  side: 'buy' | 'sell'
  price: number
  size: number
};

export type Aggregation = {
  values: number[]
  current: number
};

export type AppState = {
  productId: string
  loading: boolean
  spread: number
  aggregation: Aggregation
  asks: Order[]
  bids: Order[]
};

export type ImmutableState = ImmutableObject<AppState>;
