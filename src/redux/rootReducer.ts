import { combineReducers } from 'redux-seamless-immutable';

import appReducer, { ImmutableState as AppState } from './modules/app';


/**
 * Global App state
 */
export type State = {
  app: AppState
};

/**
 * Root reducer
 */
export default combineReducers<State>({
  app: appReducer
});
