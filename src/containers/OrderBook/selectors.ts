import { createSelector, Selector } from 'reselect';
import { State } from '../../redux/rootReducer';
import { Aggregation, Order } from '../../redux/modules/app';


const getAsks: Selector<State, Order[]> = state => state.app.asks;
const getBids: Selector<State, Order[]> = state => state.app.bids;
const getAggr: Selector<State, Aggregation> = state => state.app.aggregation;


export const asksSelector = createSelector<State, Order[], Aggregation, Order[]>(
  getAsks,
  getAggr,
  (asks, { values, current }) => asks.reduce(reduceFunc(values[current]), [] as Order[])
);

export const bidsSelector = createSelector<State, Order[], Aggregation, Order[]>(
  getBids,
  getAggr,
  (bids, { values, current }) => bids.reduce(reduceFunc(values[current]), [] as Order[])
);

function reduceFunc(aggrValue: number): (acc: Order[], Order) => Order[] {
  return (acc, order) => {
    const lastIndex = acc.length - 1;
    const prevOrder = lastIndex > 0 ? acc[lastIndex - 1] : null;

    if (prevOrder === null || Math.abs(prevOrder.price - order.price) > aggrValue) {
      const newPrice = Math.round(order.price / aggrValue) * aggrValue;

      acc.push({
        ...order,
        id: newPrice.toFixed(2),
        price: newPrice
      });
    } else {
      acc[lastIndex - 1] = {
        ...prevOrder,
        size: prevOrder.size + order.size
      };
    }

    return acc;
  };
}
