export type Order = {
  id: string
  side: 'buy' | 'sell'
  price: number
  size: number
};

export type StateProps = {
  productId: string
  loading: boolean
  spread: number
  aggregation: number
  asks: Order[]
  bids: Order[]
};

export type DispatchProps = {
  connectOrderBook: () => void
  disconnectOrderBook: () => void
};

export type OwnProps = {};

export type Props = StateProps & DispatchProps & OwnProps;
