import * as React from 'react';
import { connect } from 'react-redux';
import './styles.sss';

import { State } from '../../redux/rootReducer';
import { connectOrderBook, disconnectOrderBook } from '../../utils/gdax/actions';
import { StateProps, DispatchProps, OwnProps, Props } from './types';
import { asksSelector, bidsSelector } from './selectors';

import OrderTable from '../../components/OrderTable';
import Loader from '../../components/Loader';
import OrderHeader from '../OrderHeader/';


class OrderBook extends React.Component<Props> {
  componentDidMount() {
    this.props.connectOrderBook();
  }

  componentWillUnmount() {
    this.props.disconnectOrderBook();
  }

  render() {
    const { asks, bids, loading } = this.props;

    return loading
      ? <Loader loading/>
      : (
        <div styleName="order-book">
          <OrderHeader />
          
          <div styleName="table-wrap">
            <OrderTable orders={asks}/>
            <OrderTable orders={bids}/>
          </div>
        </div>
      );
  }
}

export default connect<StateProps, DispatchProps, OwnProps, State>(
  (state) => {
    const { values, current } = state.app.aggregation;

    return {
      ...state.app,
      asks: asksSelector(state),
      bids: bidsSelector(state),
      aggregation: values[current]
    };
  },
  { connectOrderBook, disconnectOrderBook }
)(OrderBook);
