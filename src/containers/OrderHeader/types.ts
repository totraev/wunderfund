
export type StateProps = {
  aggregation: {
    values: number[]
    current: number
  }
  spread: number
};

export type DispatchProps = {
  incAggr: () => void
  decAggr: () => void
};

export type OwnProps = {};

export type Props = StateProps & DispatchProps & OwnProps;
