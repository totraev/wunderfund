import * as React from 'react';
import { connect } from 'react-redux';
import './styles.sss';

import { incAggr, decAggr } from '../../redux/modules/app';

import { State } from '../../redux/rootReducer';
import { StateProps, DispatchProps, OwnProps, Props } from './types';

const OrderHeader: React.SFC<Props> = ({ aggregation, spread, incAggr, decAggr }) => {
  const { values, current } = aggregation;

  return (
    <div styleName="order-book-header">
      <div styleName="aggr">
        Aggregation:
        <span styleName="aggr-value">{values[current].toFixed(2)}</span>
      </div>

      <div styleName="aggr-controls">
        <button
          styleName="btn"
          disabled={current === values.length - 1}
          onClick={() => incAggr()}>
          +
        </button>
        <button
          styleName="btn"
          disabled={current === 0}
          onClick={() => decAggr()}>
          -
        </button>
      </div>

      <div styleName="spread">
        Spread:
        <span styleName="spread-value">
          {spread.toFixed(2)}
        </span>
      </div>
    </div>
  );
};

export default connect<StateProps, DispatchProps, OwnProps, State>(
  ({ app: { aggregation, spread } }) => ({ aggregation, spread }),
  { incAggr, decAggr }
)(OrderHeader);
