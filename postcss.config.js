module.exports = {
  parser: 'sugarss',
  plugins: {
    'stylelint': {},
    'postcss-use': {
      modules: [/^postcss/]
    },
    'postcss-cssnext': {},
    'postcss-reporter': {
      clearAllMessages: true
    },
    'cssnano': { autoprefixer: false }
  }
}
