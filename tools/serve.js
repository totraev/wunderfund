const browserSync = require('browser-sync')
const proxy = require('http-proxy-middleware')
const historyApiFallback = require('connect-history-api-fallback')

browserSync({
  port: 4000,
  ui: {
    port: 4001
  },
  server: {
    baseDir: 'dist',

    middleware: [
      proxy('/api', { target: 'http://localhost:3000', changeOrigin: true }),
      historyApiFallback(),
    ]
  },
  files: [
    'dist/*.html'
  ]
})
